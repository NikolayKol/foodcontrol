package com.example.nikolay.storage;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by nikolay on 02.09.15.
 */
public class DBHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "mydatabase.db";
    private static final int DATABASE_VERSION = 1;

    public static final String DIET_PROGRAMS_TABLE = "diets";
    public static final String MEALS_TABLE = "meals";
    public static final String MEALS_DIARY_TABLE = "diary";

    public static final String DIET_PROGRAM_ID_COLUMN = "id";
    public static final String IS_VISIBLE_COLUMN = "is_visible";
    public static final String RECORD_ID_COLUMN = "record_id";
    public static final String NAME_COLUMN = "name";
    public static final String NUMBER_OF_MEAL_IN_DAY_COLUMN = "number";
    public static final String TYPE_COLUMN = "amount";
    public static final String INTERVALS_COLUMN = "intervals";
    public static final String DATE_COLUMN = "date";
    public static final String DISHES_COLUMN = "dishes";
    public static final String PROTEIN_COLUMN = "protein";
    public static final String FAT_COLUMN = "fat";
    public static final String CARBOHYDRATES_COLUMN = "carb";


    private static final String DIET_TABLE_CREATE_SCRIPT = "create table " + DIET_PROGRAMS_TABLE + " ("
            + DIET_PROGRAM_ID_COLUMN + " text not null, "
            + NAME_COLUMN + " text not null, "
            + TYPE_COLUMN + " integer, "
            + INTERVALS_COLUMN + " text not null, "
            + IS_VISIBLE_COLUMN + " text not null"
            + ");";



    private static final String MEALS_TABLE_CREATE_SCRIPT = "create table " + MEALS_TABLE + " ("
            + DIET_PROGRAM_ID_COLUMN + " text not null, "
            + NUMBER_OF_MEAL_IN_DAY_COLUMN + " integer, "
            + DISHES_COLUMN + " text not null, "
            + PROTEIN_COLUMN + " real, "
            + FAT_COLUMN + " real, "
            + CARBOHYDRATES_COLUMN + " real" +
            ");";



    private static final String MEALS_DIARY_TABLE_CREATE_SCRIPT = "create table " + MEALS_DIARY_TABLE + " ("
            + DIET_PROGRAM_ID_COLUMN + " text not null, "
            + RECORD_ID_COLUMN + " text not null, "
            + NUMBER_OF_MEAL_IN_DAY_COLUMN + " integer, "
            + DATE_COLUMN + " text not null, "
            + DISHES_COLUMN + " text not null, "
            + PROTEIN_COLUMN + " real, "
            + FAT_COLUMN + " real, "
            + CARBOHYDRATES_COLUMN + " real" +
            ");";





    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DIET_TABLE_CREATE_SCRIPT);
            db.execSQL(MEALS_TABLE_CREATE_SCRIPT);
            db.execSQL(MEALS_DIARY_TABLE_CREATE_SCRIPT);
        }catch (SQLException s) {
            Log.e("create mistake", s.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
