package com.example.nikolay.storage;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.DietType;
import com.example.nikolay.entities.DietProgramMeal;
import com.example.nikolay.entities.MealRecordInDiary;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by nikolay on 02.09.15.
 */
public class Storage {
    Context context;
    SQLiteDatabase sdb;
    DBHelper dbHelper;
    SharedPreferences preferences;
    SimpleDateFormat simpleDateFormat;
    private DietProgram activeDietProgram;

    private final String DIET_PROGRAM_PREFS_NAME = "current diet program";
    private final String DIET_PROGRAM_PREFS_KEY = "diet";


    public Storage(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
        preferences = context.getSharedPreferences(DIET_PROGRAM_PREFS_NAME, Context.MODE_PRIVATE);
        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    }

    private void loadStorage() {
        sdb = dbHelper.getReadableDatabase();
    }

    public void setDietProgramActive(DietProgram dietProgramActive) {

        SharedPreferences.Editor editor = preferences.edit();

        if (dietProgramActive == null)
            editor.putString(DIET_PROGRAM_PREFS_KEY, "");
        else
            editor.putString(DIET_PROGRAM_PREFS_KEY, dietProgramActive.getUuid().toString());

        editor.commit();
        activeDietProgram = dietProgramActive;
    }

    public DietProgram getActiveDietProgram() {
        String curId = preferences.getString(DIET_PROGRAM_PREFS_KEY, "");
        if (curId.isEmpty()) return null;

        if (activeDietProgram == null)
            activeDietProgram = getDietProgram(UUID.fromString(curId));

        return activeDietProgram;
    }

    public DietProgram getDietProgram(UUID uuid) {

        if (sdb == null) loadStorage();

        String query = "select * from " + DBHelper.DIET_PROGRAMS_TABLE + " where " + DBHelper.DIET_PROGRAM_ID_COLUMN + " = '" + uuid.toString() + "'";
        Cursor cursor = sdb.rawQuery(query, null);

        boolean success = cursor.moveToFirst();

        if (!success) return null;

        String name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME_COLUMN));

        int times = cursor.getInt(cursor.getColumnIndex(DBHelper.TYPE_COLUMN));
        DietType dietType = DietType.getTypeFromTimes(times);

        String jsonIntervals = cursor.getString(cursor.getColumnIndex(DBHelper.INTERVALS_COLUMN));
        ArrayList<String> intervals = null;

        try {
            JSONArray jsonArray = new JSONArray(jsonIntervals);
            intervals = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                intervals.add(jsonArray.getString(i));
            }
        } catch (JSONException jse) {
            Log.d("json", "error in intervals reading");
        }

        cursor.close();

        return new DietProgram(uuid, name, dietType, intervals);
    }

    public ArrayList<DietProgramMeal> getMealsForDietProgram(UUID uuid) {

        if (sdb == null) loadStorage();

        Cursor cursor = sdb.rawQuery("select * from " + DBHelper.MEALS_TABLE + " where " + DBHelper.DIET_PROGRAM_ID_COLUMN + " = ?",
                new String[]{uuid.toString()});

        ArrayList<DietProgramMeal> meals = new ArrayList<>();

        if (! cursor.moveToFirst()) return meals;

        do {
            int numberOfMealInDay = cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER_OF_MEAL_IN_DAY_COLUMN));
            String dishesJson = cursor.getString(cursor.getColumnIndex(DBHelper.DISHES_COLUMN));
            ArrayList<String> dishes = new ArrayList<>();

            try {
                JSONArray dishesJsonArray = new JSONArray(dishesJson);

                for (int i = 0; i < dishesJsonArray.length(); i++) {
                    dishes.add(dishesJsonArray.getString(i));
                }

            } catch (JSONException jse) {
                Log.d("json", "error in dishes parse");
            }

            double proteins = cursor.getDouble(cursor.getColumnIndex(DBHelper.PROTEIN_COLUMN));
            double fats = cursor.getDouble(cursor.getColumnIndex(DBHelper.FAT_COLUMN));
            double carbohydrates = cursor.getDouble(cursor.getColumnIndex(DBHelper.CARBOHYDRATES_COLUMN));

            DietProgramMeal meal = new DietProgramMeal(uuid, numberOfMealInDay, dishes, proteins, fats, carbohydrates);
            meals.add(meal);

        } while (cursor.moveToNext());

        cursor.close();

        return meals;
    }

    public ArrayList<DietProgram> getVisibleDietPrograms() {

        if (sdb == null) loadStorage();

        ArrayList<DietProgram> dietPrograms = new ArrayList<>();

        String query = "select * from " + DBHelper.DIET_PROGRAMS_TABLE + " where " + DBHelper.IS_VISIBLE_COLUMN + " = 1";

        Cursor cursor = sdb.rawQuery(query, null);

        if (!cursor.moveToFirst()) return dietPrograms;
        do {
            UUID uuid = UUID.fromString(cursor.getString(cursor.getColumnIndex(DBHelper.DIET_PROGRAM_ID_COLUMN)));
            String name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME_COLUMN));
            int times = cursor.getInt(cursor.getColumnIndex(DBHelper.TYPE_COLUMN));
            DietType dietType = DietType.getTypeFromTimes(times);
            String jsonIntervals = cursor.getString(cursor.getColumnIndex(DBHelper.INTERVALS_COLUMN));
            ArrayList<String> intervals = null;

            try {
                JSONArray jsonArray = new JSONArray(jsonIntervals);
                intervals = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    intervals.add(jsonArray.getString(i));
                }
            } catch (JSONException jse) {
                Log.d("json", "error in intervals reading");
            }

            DietProgram dietProgram = new DietProgram(uuid, name, dietType, intervals);
            dietPrograms.add(dietProgram);

        } while (cursor.moveToNext());

        cursor.close();
        return dietPrograms;
    }

    public ArrayList<MealRecordInDiary> getDiaryRecordsForDietProgram(UUID programId) {

        if (sdb == null) loadStorage();

        Cursor cursor = sdb.rawQuery("select * from " + DBHelper.MEALS_DIARY_TABLE + " where " + DBHelper.DIET_PROGRAM_ID_COLUMN + " = ?",
                new String[]{programId.toString()});

        ArrayList<MealRecordInDiary> records = new ArrayList<>();

        if (! cursor.moveToFirst()) return records;

        do {
            int numberOfMealInDay = cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER_OF_MEAL_IN_DAY_COLUMN));
            String dateString = cursor.getString(cursor.getColumnIndex(DBHelper.DATE_COLUMN));

            Date date = new Date();

            String dishesJson = cursor.getString(cursor.getColumnIndex(DBHelper.DISHES_COLUMN));
            ArrayList<String> dishes = new ArrayList<>();

            try {
                date = simpleDateFormat.parse(dateString);

                JSONArray jsonArray = new JSONArray(dishesJson);

                for (int i = 0; i < jsonArray.length(); i++) {
                    dishes.add(jsonArray.getString(i));
                }

            } catch (ParseException pe) {
                Log.d("date", "date parse exception");
            } catch (JSONException jse) {
                Log.d("json", "error in parse dishes");
            }

            double proteins = cursor.getDouble(cursor.getColumnIndex(DBHelper.PROTEIN_COLUMN));
            double fats = cursor.getDouble(cursor.getColumnIndex(DBHelper.FAT_COLUMN));
            double carbohydrates = cursor.getDouble(cursor.getColumnIndex(DBHelper.CARBOHYDRATES_COLUMN));

            records.add(new MealRecordInDiary(programId, numberOfMealInDay, date, dishes, proteins, carbohydrates, fats));

        } while (cursor.moveToNext());

        cursor.close();

        return records;
    }

    public ArrayList<MealRecordInDiary> getAllDiaryRecords() {
        if (sdb == null) loadStorage();

        Cursor cursor = sdb.rawQuery("select * from " + DBHelper.MEALS_DIARY_TABLE
                        + " order by " + DBHelper.DIET_PROGRAM_ID_COLUMN,
                null);

        ArrayList<MealRecordInDiary> records = new ArrayList<>();

        if (! cursor.moveToFirst()) return records;

        do {
            UUID programId = UUID.fromString(cursor.getString(cursor.getColumnIndex(DBHelper.DIET_PROGRAM_ID_COLUMN)));

            UUID recordId = UUID.fromString(cursor.getString(cursor.getColumnIndex(DBHelper.RECORD_ID_COLUMN)));

            int numberOfMealInDay = cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER_OF_MEAL_IN_DAY_COLUMN));


            String dateString = cursor.getString(cursor.getColumnIndex(DBHelper.DATE_COLUMN));

            Date date = new Date();

            String dishesJson = cursor.getString(cursor.getColumnIndex(DBHelper.DISHES_COLUMN));
            ArrayList<String> dishes = new ArrayList<>();

            try {
                date = simpleDateFormat.parse(dateString);

                JSONArray jsonArray = new JSONArray(dishesJson);

                for (int i = 0; i < jsonArray.length(); i++) {
                    dishes.add(jsonArray.getString(i));
                }

            } catch (ParseException pe) {
                Log.d("date", "date parse exception");
            } catch (JSONException jse) {
                Log.d("json", "error in parse dishes");
            }

            double proteins = cursor.getDouble(cursor.getColumnIndex(DBHelper.PROTEIN_COLUMN));
            double fats = cursor.getDouble(cursor.getColumnIndex(DBHelper.FAT_COLUMN));
            double carbohydrates = cursor.getDouble(cursor.getColumnIndex(DBHelper.CARBOHYDRATES_COLUMN));

            records.add(new MealRecordInDiary(recordId, programId, numberOfMealInDay, date, dishes, proteins, carbohydrates, fats));

        } while (cursor.moveToNext());

        cursor.close();

        return records;

    }

    public void addDietProgram(DietProgram dietProgram, ArrayList<DietProgramMeal> meals, boolean active) {

        if (sdb == null) loadStorage();

        try {
            if (active) {
                setDietProgramActive(dietProgram);
            }

            ContentValues dietValues = new ContentValues();

            dietValues.put(DBHelper.DIET_PROGRAM_ID_COLUMN, dietProgram.getUuid().toString());
            dietValues.put(DBHelper.NAME_COLUMN, dietProgram.getName());
            dietValues.put(DBHelper.TYPE_COLUMN, dietProgram.getType().getTimes());

            JSONArray jsonArray = new JSONArray(dietProgram.getIntervals());
            String intervals = jsonArray.toString();

            dietValues.put(DBHelper.INTERVALS_COLUMN, intervals);
            dietValues.put(DBHelper.IS_VISIBLE_COLUMN, 1);

            sdb.insert(DBHelper.DIET_PROGRAMS_TABLE, null, dietValues);

            for (DietProgramMeal meal : meals) {

                ContentValues values = new ContentValues();

                values.put(DBHelper.DIET_PROGRAM_ID_COLUMN, dietProgram.getUuid().toString());
                values.put(DBHelper.NUMBER_OF_MEAL_IN_DAY_COLUMN, meal.getNumberOfMealInDay());


                JSONArray dishesJsonArray = new JSONArray(meal.getDishes());
                String dishes = dishesJsonArray.toString();

                values.put(DBHelper.DISHES_COLUMN, dishes);
                values.put(DBHelper.PROTEIN_COLUMN, meal.getProteins());
                values.put(DBHelper.FAT_COLUMN, meal.getFats());
                values.put(DBHelper.CARBOHYDRATES_COLUMN, meal.getCarbohydrates());

                sdb.insert(DBHelper.MEALS_TABLE, null, values);
            }

        } catch (Exception e) {
            Log.d("sql", e.getMessage());
        }

    }

    public void addMealRecordInDiary(MealRecordInDiary recordInDiary) {

        if (sdb == null) loadStorage();

        ContentValues values = new ContentValues();

        JSONArray jsonArray = new JSONArray(recordInDiary.getDishes());
        String dishes = jsonArray.toString();

        values.put(DBHelper.DIET_PROGRAM_ID_COLUMN, recordInDiary.getDietProgramId().toString());
        values.put(DBHelper.RECORD_ID_COLUMN, recordInDiary.getRecordId().toString());
        values.put(DBHelper.NUMBER_OF_MEAL_IN_DAY_COLUMN, recordInDiary.getNumberOfMealInDay());
        values.put(DBHelper.DATE_COLUMN, simpleDateFormat.format(recordInDiary.getDate()));
        values.put(DBHelper.DISHES_COLUMN, dishes);
        values.put(DBHelper.PROTEIN_COLUMN, recordInDiary.getProteins());
        values.put(DBHelper.FAT_COLUMN, recordInDiary.getFats());
        values.put(DBHelper.CARBOHYDRATES_COLUMN, recordInDiary.getCarbohydrates());

        long  res = sdb.insert(DBHelper.MEALS_DIARY_TABLE, null, values);
        Log.d("sql", Long.toString(res));
    }

    public void updateMealRecordInDiary(MealRecordInDiary recordInDiary) {

        if (sdb == null) loadStorage();

        sdb.execSQL("delete from " + DBHelper.MEALS_DIARY_TABLE + " where " + DBHelper.RECORD_ID_COLUMN + " = '" + recordInDiary.getRecordId().toString() + "'");

        addMealRecordInDiary(recordInDiary);

    }

    public void removeDietProgram(UUID uuid) {
        String query = "delete from " + DBHelper.DIET_PROGRAMS_TABLE + " where " + DBHelper.DIET_PROGRAM_ID_COLUMN + " = '" + uuid.toString() + "'";
        sdb.execSQL(query);
    }

    public void hideDietProgram(UUID uuid) {
        String query = "update " + DBHelper.DIET_PROGRAMS_TABLE + " set " + DBHelper.IS_VISIBLE_COLUMN + " = 0"
                + " where " +DBHelper.DIET_PROGRAM_ID_COLUMN + " = '" + uuid.toString() + "'";
        sdb.execSQL(query);
    }
}
