package com.example.nikolay.entities;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by nikolay on 06.09.15.
 */
public class DietProgram implements Parcelable {

    private UUID uuid;
    private String name;
    private DietType type;
    private ArrayList<String> intervals;

    public static final Parcelable.Creator<DietProgram> CREATOR = new Creator<DietProgram>() {
        @Override
        public DietProgram createFromParcel(Parcel source) {
            return new DietProgram(source);
        }

        @Override
        public DietProgram[] newArray(int size) {
            return new DietProgram[size];
        }
    };

    public DietProgram() {
        uuid = UUID.randomUUID();
        intervals = new ArrayList<String>();
    }

    public DietProgram(String name, DietType type, ArrayList<String> intervals) {
        this.name = name;
        this.type = type;
        this.intervals = intervals;
        uuid = UUID.randomUUID();
    }

    public DietProgram(UUID uuid, String name, DietType type, ArrayList<String> intervals) {
        this.uuid = uuid;
        this.name = name;
        this.type = type;
        this.intervals = intervals;
    }

    public DietProgram(Parcel sourse) {
        uuid = UUID.fromString(sourse.readString());
        name = sourse.readString();
        type = DietType.getTypeFromTimes(sourse.readInt());
        intervals = new ArrayList<String>();
        sourse.readStringList(intervals);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uuid.toString());
        dest.writeString(name);
        dest.writeInt(type.getTimes());
        dest.writeStringList(intervals);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DietType getType() {
        return type;
    }

    public void setType(DietType type) {
        this.type = type;
    }

    public ArrayList<String> getIntervals() {
        return intervals;
    }

    public void setIntervals(ArrayList<String> intervals) {
        this.intervals = intervals;
    }
}
