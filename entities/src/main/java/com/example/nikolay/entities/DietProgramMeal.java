package com.example.nikolay.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by nikolay on 06.09.15.
 */
public class DietProgramMeal implements Parcelable {

    private UUID dietProgramId;
    private int numberOfMealInDay;
    private ArrayList<String> dishes;
    private double proteins;
    private double carbohydrates;
    private double fats;

    public static final Parcelable.Creator<DietProgramMeal> CREATOR = new Creator<DietProgramMeal>() {
        @Override
        public DietProgramMeal createFromParcel(Parcel source) {
            return new DietProgramMeal(source);
        }

        @Override
        public DietProgramMeal[] newArray(int size) {
            return new DietProgramMeal[size];
        }
    };

    public DietProgramMeal(UUID dietProgramId, int numberOfMealInDay, ArrayList<String> dishes, double proteins, double fats, double carbohydrates) {
        this.dietProgramId = dietProgramId;
        this.numberOfMealInDay = numberOfMealInDay;
        this.dishes = dishes;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
    }

    public DietProgramMeal(Parcel source) {
        dietProgramId = UUID.fromString(source.readString());
        numberOfMealInDay = source.readInt();
        dishes = new ArrayList<String>();
        source.readStringList(dishes);
        proteins = source.readDouble();
        fats = source.readDouble();
        carbohydrates = source.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dietProgramId.toString());
        dest.writeInt(numberOfMealInDay);
        dest.writeStringList(dishes);
        dest.writeDouble(proteins);
        dest.writeDouble(fats);
        dest.writeDouble(carbohydrates);
    }

    public UUID getDietProgramId() {
        return dietProgramId;
    }

    public int getNumberOfMealInDay() {
        return numberOfMealInDay;
    }

    public ArrayList<String> getDishes() {
        return dishes;
    }

    public double getProteins() {
        return proteins;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getFats() {
        return fats;
    }
}
