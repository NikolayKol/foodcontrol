package com.example.nikolay.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by nikolay on 06.09.15.
 */
public class MealRecordInDiary implements Parcelable{

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");

    private UUID recordId;
    private UUID dietProgramId;
    private int numberOfMealInDay;
    private Date date;
    private ArrayList<String> dishes;
    private double proteins;
    private double carbohydrates;
    private double fats;


    public static final Parcelable.Creator<MealRecordInDiary> CREATOR = new Creator<MealRecordInDiary>() {
        @Override
        public MealRecordInDiary createFromParcel(Parcel source) {
            return new MealRecordInDiary(source);
        }

        @Override
        public MealRecordInDiary[] newArray(int size) {
            return new MealRecordInDiary[size];
        }
    };


    public MealRecordInDiary(UUID recordId, UUID dietProgramId, int numberOfMealInDay, Date date, ArrayList<String> dishes, double proteins, double carbohydrates, double fats) {
        this.recordId = recordId;
        this.dietProgramId = dietProgramId;
        this.numberOfMealInDay = numberOfMealInDay;
        this.date = date;
        this.dishes = dishes;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
    }

    public MealRecordInDiary(UUID dietProgramId, int numberOfMealInDay, Date date, ArrayList<String> dishes, double proteins, double carbohydrates, double fats) {
        this.dietProgramId = dietProgramId;
        this.numberOfMealInDay = numberOfMealInDay;
        this.date = date;
        this.dishes = dishes;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
        this.recordId = UUID.randomUUID();
    }

    public MealRecordInDiary(UUID dietProgramId, int numberOfMealInDay, ArrayList<String> dishes, double proteins, double carbohydrates, double fats) {
        this.dietProgramId = dietProgramId;
        this.numberOfMealInDay = numberOfMealInDay;
        this.dishes = dishes;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
        this.date = new Date();
        this.recordId = UUID.randomUUID();
    }

    public MealRecordInDiary(Parcel source) {
        recordId = UUID.fromString(source.readString());
        dietProgramId = UUID.fromString(source.readString());
        numberOfMealInDay = source.readInt();
        try {
            date = dateFormat.parse(source.readString());
        }catch (ParseException pe) {
            Log.d("date", "date parse exeption");
        }
        dishes = new ArrayList<String>();
        source.readStringList(dishes);
        proteins = source.readDouble();
        carbohydrates = source.readDouble();
        fats = source.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(recordId.toString());
        dest.writeString(dietProgramId.toString());
        dest.writeInt(numberOfMealInDay);
        dest.writeString(dateFormat.format(date));
        dest.writeStringList(dishes);
        dest.writeDouble(proteins);
        dest.writeDouble(carbohydrates);
        dest.writeDouble(fats);
    }

    public UUID getRecordId() {
        return recordId;
    }

    public UUID getDietProgramId() {
        return dietProgramId;
    }

    public int getNumberOfMealInDay() {
        return numberOfMealInDay;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<String> getDishes() {
        return dishes;
    }

    public double getProteins() {
        return proteins;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getFats() {
        return fats;
    }
}
