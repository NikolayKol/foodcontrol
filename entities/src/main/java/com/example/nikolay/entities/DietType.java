package com.example.nikolay.entities;

/**
 * Created by nikolay on 06.09.15.
 */
public enum DietType {
    ThreeTimes(3),
    FiveTimes(5);

    private int times;

    DietType(int times) {
        this.times = times;
    }

    public int getTimes() {
        return times;
    }

    public static DietType getTypeFromTimes(int times) {
        if (times == 3) return ThreeTimes;
        else if (times == 5) return FiveTimes;
        else return null;
    }
}
