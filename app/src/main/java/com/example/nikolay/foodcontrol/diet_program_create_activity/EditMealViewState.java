package com.example.nikolay.foodcontrol.diet_program_create_activity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by nikolay on 23.09.15.
 */
public class EditMealViewState implements Parcelable{

    private Integer mealNameId;
    private double proteins;
    private double carbohydrates;
    private double fats;
    private ArrayList<String> dishes;
    private String interval;

    public static final Parcelable.Creator<EditMealViewState> CREATOR = new Creator<EditMealViewState>() {
        @Override
        public EditMealViewState createFromParcel(Parcel source) {
            return new EditMealViewState(source);
        }

        @Override
        public EditMealViewState[] newArray(int size) {
            return new EditMealViewState[size];
        }
    };

    public EditMealViewState(Integer mealNameId, double proteins, double carbohydrates, double fats, ArrayList<String> dishes, String interval) {
        this.mealNameId = mealNameId;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
        this.dishes = dishes;
        this.interval = interval;
    }

    public EditMealViewState(double proteins, double carbohydrates, double fats, ArrayList<String> dishes, String interval) {
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
        this.dishes = dishes;
        this.interval = interval;
    }

    public EditMealViewState(Parcel source) {
        mealNameId = source.readInt();
        proteins = source.readDouble();
        carbohydrates = source.readDouble();
        fats = source.readDouble();
        dishes = new ArrayList<String>();
        source.readStringList(dishes);
        interval = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mealNameId);
        dest.writeDouble(proteins);
        dest.writeDouble(carbohydrates);
        dest.writeDouble(fats);
        dest.writeStringList(dishes);
        dest.writeString(interval);
    }

    public Integer getMealNameId() {
        return mealNameId;
    }

    public double getProteins() {
        return proteins;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getFats() {
        return fats;
    }

    public ArrayList<String> getDishes() {
        return dishes;
    }

    public String getInterval() {
        return interval;
    }


    @Override
    public String toString() {
        return "EditMealViewState{" +
                "mealNameId=" + mealNameId +
                ", proteins=" + proteins +
                ", carbohydrates=" + carbohydrates +
                ", fats=" + fats +
                ", dishes=" + dishes +
                ", interval='" + interval + '\'' +
                '}';
    }
}
