package com.example.nikolay.foodcontrol.diet_program_create_activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nikolay.foodcontrol.R;

import java.util.ArrayList;

/**
 * Created by nikolay on 23.09.15.
 */
public class EditMealView extends FrameLayout {

    private LinearLayout layoutForDishes;
    private Button addDishBtn;
    private View contentView;
    private Integer mealNameId;
    private EditText editP;
    private EditText editF;
    private EditText editC;

    public EditMealView(Context context) {
        super(context);
        initView();
    }

    public EditMealView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public EditMealView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public EditMealView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        contentView = inflate(getContext(), R.layout.edit_meal_element, null);

        if (getId() == R.id.breakfastView) {
            mealNameId = R.string.breakfast;
            ((TextView) contentView.findViewById(R.id.mealName)).setText(mealNameId);
            contentView.findViewById(R.id.editInterval).setVisibility(INVISIBLE);
        } else if (getId() == R.id.brunchView) {
            mealNameId = R.string.brunch;
            ((TextView) contentView.findViewById(R.id.mealName)).setText(mealNameId);
        } else if (getId() == R.id.lunchView) {
            mealNameId = R.string.lunch;
            ((TextView) contentView.findViewById(R.id.mealName)).setText(mealNameId);
        } else if (getId() == R.id.dinnerView) {
            mealNameId = R.string.dinner;
            ((TextView) contentView.findViewById(R.id.mealName)).setText(mealNameId);
        } else if (getId() == R.id.supperView) {
            mealNameId = R.string.supper;
            ((TextView) contentView.findViewById(R.id.mealName)).setText(mealNameId);
        }

        editP = (EditText) contentView.findViewById(R.id.editProteins);
        editF = (EditText) contentView.findViewById(R.id.editFats);
        editC = (EditText) contentView.findViewById(R.id.editCarbohydrates);

        layoutForDishes = (LinearLayout) contentView.findViewById(R.id.linearLayoutForDishes);

        addDishBtn = (Button) contentView.findViewById(R.id.addDishButton);
        addDishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                EditText editText = new EditText(getContext());
                editText.setLayoutParams(layoutParams);
                editText.setMaxLines(1);
                editText.requestFocus();
                editText.setHint(R.string.edit_dish_hint);
                layoutForDishes.addView(editText);
            }
        });

        addView(contentView);
    }

    public void setState(EditMealViewState state) {
        if (state.getProteins() != -1) {
            //((EditText) contentView.findViewById(R.id.editProteins)).setText(Double.toString(state.getProteins()));
            editP.setText(Double.toString(state.getProteins()));
        }

        if (state.getFats() != -1) {
            //((EditText) contentView.findViewById(R.id.editFats)).setText(Double.toString(state.getFats()));
            editF.setText(Double.toString(state.getFats()));
        }

        if (state.getCarbohydrates() != -1) {
            //((EditText) contentView.findViewById(R.id.editCarbohydrates)).setText(Double.toString(state.getCarbohydrates()));
            editC.setText(Double.toString(state.getCarbohydrates()));
        }

        if (state.getInterval() != null) {
            ((EditText) contentView.findViewById(R.id.editInterval)).setText(state.getInterval());
        }

        for (int j = 0; j < state.getDishes().size(); j++) {

            EditText editText;

            if (j == 0) editText = ((EditText) contentView.findViewById(R.id.editFirstDish));
            else {
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                editText = new EditText(getContext());
                editText.setLayoutParams(layoutParams);
                editText.setMaxLines(1);
                layoutForDishes.addView(editText);
            }

            editText.setText(state.getDishes().get(j));

        }
    }

    public EditMealViewState getState() {
        Double proteins;
        Double fats;
        Double carbohydrates;

        try {
            EditText p = (EditText) contentView.findViewById(R.id.editProteins);
            proteins = Double.parseDouble(p.getText().toString());
        } catch (NumberFormatException nfe) {
            proteins = -1.0;
        }

        try {
            EditText f = (EditText) contentView.findViewById(R.id.editFats);
            fats = Double.parseDouble(f.getText().toString());
        } catch (NumberFormatException nfe) {
            fats = -1.0;
        }

        try {
            EditText c = (EditText) contentView.findViewById(R.id.editCarbohydrates);
            carbohydrates = Double.parseDouble(c.getText().toString());
        } catch (NumberFormatException nfe) {
            carbohydrates = -1.0;
        }

        ArrayList<String> dishes = new ArrayList<>();

        for (int j = 0; j < layoutForDishes.getChildCount(); j++) {
            EditText editDish = (EditText) layoutForDishes.getChildAt(j);
            String dish = editDish.getText().toString();
            if (!dish.isEmpty()) {
                dishes.add(dish);
            }
        }

        EditText editInterval = (EditText) contentView.findViewById(R.id.editInterval);

        String interval = editInterval.getVisibility() == INVISIBLE ? null : editInterval.getText().toString();


        return new EditMealViewState(mealNameId, proteins, carbohydrates, fats, dishes, interval);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        SavedState savedState = new SavedState(BaseSavedState.EMPTY_STATE);
        EditMealViewState state = getState();
        Log.i("bzzzz", "state to save ------------------");
        Log.i("bzzzz", state.toString());
        savedState.setEditMealViewState(state);
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        EditMealViewState editMealViewState = savedState.getEditMealViewState();

        Log.i("bzzzz", "state restored for " + ((TextView)contentView.findViewById(R.id.mealName)).getText() );
        Log.i("bzzzz", editMealViewState.toString());
        setState(editMealViewState);
    }

    /**
    * Override to prevent freezing of any views created by the adapter.
     */
    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    /**
     * Override to prevent thawing of any views created by the adapter.
     */
    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    static class SavedState extends BaseSavedState {

        EditMealViewState editMealViewState;

        private SavedState(Parcel source) {
            super(source);
            editMealViewState = source.readParcelable(EditMealView.class.getClassLoader());
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source) {
                return new SavedState(source);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(editMealViewState, 0);
        }

        public EditMealViewState getEditMealViewState() {
            return editMealViewState;
        }

        public void setEditMealViewState(EditMealViewState editMealViewState) {
            this.editMealViewState = editMealViewState;
        }
    }
}
