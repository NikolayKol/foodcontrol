package com.example.nikolay.foodcontrol.diary_activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.MealRecordInDiary;
import com.example.nikolay.foodcontrol.App;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.foodcontrol.edit_diary_record_activity.EditDiaryRecordActivity;
import com.example.nikolay.foodcontrol.archive_activity.ArchiveActivity;
import com.example.nikolay.foodcontrol.diet_program_create_activity.DietProgramCreateActivity;
import com.example.nikolay.foodcontrol.diet_show_activity.DietShowActivity;
import com.example.nikolay.usecases.UseCases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


public class DiaryActivity extends Activity {

    private UseCases useCases;
    private TextView nameView;
    private ImageButton addRecord;
    private ImageButton addDietProgram;
    private ImageButton archive;
    private ListView listView;
    private DiaryListAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        useCases = ((App) getApplication()).getUseCases();
        setContentView(R.layout.activity_diary);

        nameView = (TextView) findViewById(R.id.dietProgramName);
        nameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryActivity.this, DietShowActivity.class);
                intent.putExtra(DietShowActivity.KEY_FOR_SHOWED_PROGRAM,
                        useCases.getActiveDietProgram().getUuid().toString());
                startActivity(intent);
            }
        });


        addRecord = (ImageButton) findViewById(R.id.addRecord);
        addRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryActivity.this, EditDiaryRecordActivity.class);
                int nextNumberOfMealInDay = getNextNumberOfMealInDay();
                intent.putExtra(EditDiaryRecordActivity.CURRENT_MEAL_NUMBER_KEY, nextNumberOfMealInDay);
                startActivity(intent);
            }
        });

        addDietProgram = (ImageButton) findViewById(R.id.addProgram);
        addDietProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryActivity.this, DietProgramCreateActivity.class);
                startActivity(intent);
            }
        });

        archive = (ImageButton) findViewById(R.id.archive);
        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryActivity.this, ArchiveActivity.class);
                startActivity(intent);
            }
        });

        adapter = new DiaryListAdapter(this);
        listView = (ListView) findViewById(R.id.diaryListView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DiaryActivity.this, EditDiaryRecordActivity.class);

                Map.Entry<MealRecordInDiary, DietProgram> pair = (Map.Entry<MealRecordInDiary, DietProgram>) adapter.getItem(position);

                intent.putExtra(EditDiaryRecordActivity.EDITED_RECORD_KEY, pair.getKey());
                intent.putExtra(EditDiaryRecordActivity.EDITED_RECORD_PROGRAM_KEY, pair.getValue());

                startActivity(intent);
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    private int getNextNumberOfMealInDay() {
        if (adapter.getCount() == 0
                || !((Map.Entry<MealRecordInDiary, DietProgram>)adapter.getItem(0)).getValue().getUuid()
                .equals(useCases.getActiveDietProgram().getUuid())) {
            return 0;
        } else {
            int number = (((Map.Entry<MealRecordInDiary, DietProgram>)adapter.getItem(0)).getKey()
                    .getNumberOfMealInDay() + 1) % (useCases.getActiveDietProgram().getType().getTimes());
            return number;
        }
    }

    private void showProgressBar(boolean show) {

        if (show) {
            listView.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (useCases.isFirstAppUsing()) {
            Intent intent = new Intent(DiaryActivity.this, DietProgramCreateActivity.class);
            intent.putExtra(DietProgramCreateActivity.CREATE_FIRST_PROGRAM_FLAG, true);
            startActivity(intent);
            finish();
        } else {

            nameView.setText(useCases.getActiveDietProgram().getName());
            DiaryDataGetter diaryDataGetter = new DiaryDataGetter();
            diaryDataGetter.execute();
        }
    }

    class DiaryDataGetter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(Void... params) {

            List<Map.Entry<MealRecordInDiary, DietProgram>> data = new ArrayList<>(useCases.getDiaryData().entrySet());

            Comparator<Map.Entry<MealRecordInDiary, DietProgram>> comparatorByDate = new Comparator<Map.Entry<MealRecordInDiary, DietProgram>>() {
                @Override
                public int compare(Map.Entry<MealRecordInDiary, DietProgram> lhs, Map.Entry<MealRecordInDiary, DietProgram> rhs) {
                    if (rhs.getKey().getDate().before(lhs.getKey().getDate())) return -1;
                    else return 1;
                }
            };

            Collections.sort(data, comparatorByDate);

            adapter.setData(data);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            showProgressBar(false);
        }
    }
}
