package com.example.nikolay.foodcontrol.diet_program_create_activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.DietType;
import com.example.nikolay.entities.DietProgramMeal;
import com.example.nikolay.foodcontrol.App;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.foodcontrol.diary_activity.DiaryActivity;
import com.example.nikolay.usecases.UseCases;

import java.util.ArrayList;
import java.util.UUID;


public class DietProgramCreateActivity extends Activity {

    public static final String SAMPLE_DIET_PROGRAM_ID_KEY = "based_diet";
    public static final String CREATE_FIRST_PROGRAM_FLAG = "flag";

    UseCases useCases;

    DietProgram enteredDietProgram;
    ArrayList<DietProgramMeal> enteredMeals;
    Switch aSwitch;
    ImageButton saveBtn;
    LinearLayout layoutForMeals;
    ScrollView scrollView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_diet_program_create);

        useCases = ((App) getApplication()).getUseCases();

        aSwitch = (Switch) findViewById(R.id.switchDietType);

        saveBtn = (ImageButton) findViewById(R.id.saveDietProgramButton);

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar2);

        layoutForMeals = (LinearLayout) findViewById(R.id.layoutForCreateMeals);

        String basedProgramId = getIntent().getStringExtra(SAMPLE_DIET_PROGRAM_ID_KEY);

        if (basedProgramId != null) {
            StorageDealer storageDealer = new StorageDealer(StorageDealer.GET_SAMPLE_PROGRAM_TASK);
            storageDealer.execute(basedProgramId);
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                int visible;

                if (isChecked) visible = View.VISIBLE;
                else visible = View.GONE;

                layoutForMeals.getChildAt(3).setVisibility(visible);
                layoutForMeals.getChildAt(1).setVisibility(visible);
            }
        });
    }

    public void save() {
        collectEnteredData();

        if (isValidDietProgram()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            final EditText editText = new EditText(this);

            builder.setView(editText);
            builder.setTitle(R.string.diet_name_dialog_title);
            builder.setMessage(R.string.diet_name_dialog_message);
            builder.setNegativeButton(R.string.cancel, null);
            builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    String name = editText.getText().toString();
                    if (name.isEmpty()) {
                        Toast.makeText(DietProgramCreateActivity.this, R.string.save_notice, Toast.LENGTH_LONG).show();
                    } else {
                        enteredDietProgram.setName(name);
                        StorageDealer storageDealer = new StorageDealer(StorageDealer.SAVE_PROGRAM_TASK);
                        storageDealer.execute();
                    }

                }
            });

            builder.show();

        } else {
            Toast.makeText(this, R.string.set_all_fields_notice, Toast.LENGTH_LONG).show();
        }

    }

    private void setSampleContent(DietProgram enteredDietProgram, ArrayList<DietProgramMeal> enteredMeals) {

        ArrayList<EditMealViewState> states = new ArrayList<>();

        for (DietProgramMeal meal : enteredMeals) {

            String interval = meal.getNumberOfMealInDay() == 0 ? null : enteredDietProgram.getIntervals().get(meal.getNumberOfMealInDay() - 1);

            EditMealViewState state = new EditMealViewState(meal.getProteins(), meal.getCarbohydrates(), meal.getFats(),
                    meal.getDishes(), interval);

            states.add(state);
        }

        ((EditMealView) findViewById(R.id.breakfastView)).setState(states.get(0));

        if (enteredDietProgram.getType() == DietType.ThreeTimes) {
            ((EditMealView) findViewById(R.id.lunchView)).setState(states.get(1));
            ((EditMealView) findViewById(R.id.supperView)).setState(states.get(2));
        } else {
            ((EditMealView) findViewById(R.id.brunchView)).setState(states.get(1));
            ((EditMealView) findViewById(R.id.lunchView)).setState(states.get(2));
            ((EditMealView) findViewById(R.id.dinnerView)).setState(states.get(3));
            ((EditMealView) findViewById(R.id.supperView)).setState(states.get(4));
            aSwitch.setChecked(true);
        }

    }

    private void collectEnteredData() {

        enteredDietProgram = new DietProgram();
        enteredMeals = new ArrayList<DietProgramMeal>();

        if (aSwitch.isChecked()) enteredDietProgram.setType(DietType.FiveTimes);
        else enteredDietProgram.setType(DietType.ThreeTimes);

        for (int i = 0; i < layoutForMeals.getChildCount(); ) {

            EditMealViewState state = ((EditMealView) layoutForMeals.getChildAt(i)).getState();

            if (state.getInterval() != null)
                enteredDietProgram.getIntervals().add(state.getInterval());

            int numberOfMealInDay = 0;

            if (state.getMealNameId() == R.string.breakfast) {
                numberOfMealInDay = 0;
            } else if (state.getMealNameId() == R.string.brunch) {
                numberOfMealInDay = 1;
            } else if (state.getMealNameId() == R.string.lunch) {
                if (enteredDietProgram.getType() == DietType.ThreeTimes)
                    numberOfMealInDay = 1;
                else numberOfMealInDay = 2;
            } else if (state.getMealNameId() == R.string.dinner) {
                numberOfMealInDay = 3;
            } else if (state.getMealNameId() == R.string.supper) {
                if (enteredDietProgram.getType() == DietType.ThreeTimes)
                    numberOfMealInDay = 2;
                else numberOfMealInDay = 4;
            }

            DietProgramMeal meal = new DietProgramMeal(enteredDietProgram.getUuid(),
                    numberOfMealInDay, state.getDishes(), state.getProteins(), state.getFats(), state.getCarbohydrates());
            enteredMeals.add(meal);

            if (enteredDietProgram.getType() == DietType.ThreeTimes) i += 2;
            else i++;
        }

    }

    private boolean isValidDietProgram() {

        if (enteredDietProgram.getIntervals().size() + 1 != enteredDietProgram.getType().getTimes()) return false;

        boolean emptyIntervals = false;

        for (String interval : enteredDietProgram.getIntervals()) {
            emptyIntervals |= interval.isEmpty();
        }

        if (emptyIntervals) return false;

        if (enteredDietProgram.getType().getTimes() != enteredMeals.size()) return false;

        for (DietProgramMeal meal : enteredMeals) {
            if (meal.getDishes().isEmpty()) return false;

            if (meal.getProteins() == -1 || meal.getFats() == -1 || meal.getCarbohydrates() == -1) return false;
        }

        return true;
    }

    private void showProgressBar(boolean show) {
        if (show) {
            scrollView.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
        else {
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    class StorageDealer extends AsyncTask<String, Void, Void> {

        static final int SAVE_PROGRAM_TASK = 0;
        static final int GET_SAMPLE_PROGRAM_TASK = 1;

        int task;

        public StorageDealer(int task) {
            this.task = task;
        }

        private void getSampleProgram(String id) {
            enteredDietProgram = useCases.getDietProgram(UUID.fromString(id));
            enteredMeals = useCases.getMealsForDietProgram(UUID.fromString(id));
        }

        private void saveProgram() {
            useCases.addDietProgramToStorage(enteredDietProgram, enteredMeals,
                    getIntent().getStringExtra(SAMPLE_DIET_PROGRAM_ID_KEY) == null);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (task == GET_SAMPLE_PROGRAM_TASK) {
                setSampleContent(enteredDietProgram, enteredMeals);
                showProgressBar(false);
            } else if (task == SAVE_PROGRAM_TASK){
                if (getIntent().getBooleanExtra(CREATE_FIRST_PROGRAM_FLAG, false)) {
                    Intent intent = new Intent(DietProgramCreateActivity.this, DiaryActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            if (task == GET_SAMPLE_PROGRAM_TASK) {
                getSampleProgram(params[0]);
            } else if (task == SAVE_PROGRAM_TASK) {
                saveProgram();
            }
            return null;
        }
    }
}
