package com.example.nikolay.foodcontrol.archive_activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.foodcontrol.diet_show_activity.DietShowActivity;

import java.util.ArrayList;

/**
 * Created by nikolay on 14.09.15.
 */
public class ArchiveListAdapter extends BaseAdapter {

    ArrayList<DietProgram> dietPrograms;
    Context context;

    public ArchiveListAdapter(Context context) {
        this.context = context;
    }

    public void setDietPrograms(ArrayList<DietProgram> dietPrograms) {
        this.dietPrograms = dietPrograms;
    }

    @Override
    public int getCount() {

        if (dietPrograms == null) return 0;

        return dietPrograms.size();
    }

    @Override
    public Object getItem(int position) {
        return dietPrograms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.archive_listview_item, null);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = v.getTag().toString();
                    Intent intent = new Intent(context, DietShowActivity.class);
                    intent.putExtra(DietShowActivity.KEY_FOR_SHOWED_PROGRAM, id);
                    context.startActivity(intent);
                }
            });
        }

        DietProgram dietProgram = dietPrograms.get(position);

        convertView.setTag(dietProgram.getUuid().toString());

        ((TextView) convertView.findViewById(R.id.textViewDietName)).setText(dietProgram.getName());

        return convertView;
    }
}
