package com.example.nikolay.foodcontrol.diary_activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.MealRecordInDiary;
import com.example.nikolay.foodcontrol.R;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by nikolay on 21.09.15.
 */
public class DiaryListAdapter extends BaseAdapter {

    Context context;
    List<Map.Entry<MealRecordInDiary, DietProgram>> data;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh.mm.ss");

    public DiaryListAdapter(Context context, List<Map.Entry<MealRecordInDiary, DietProgram>> data) {
        this.context = context;
        this.data = data;
    }

    public DiaryListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<Map.Entry<MealRecordInDiary, DietProgram>> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MealRecordInDiary recordInDiary = data.get(position).getKey();
        DietProgram dietProgram = data.get(position).getValue();

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.diary_record, null);
        }

        ((TextView)convertView.findViewById(R.id.textViewDietName)).setText(dietProgram.getName());

        float partOfDay = ((float) recordInDiary.getNumberOfMealInDay()) / (dietProgram.getType().getTimes() - 1);
        ((TextView)convertView.findViewById(R.id.textViewMealName)).setText(getMealName(partOfDay));

        TextView textViewP = (TextView) convertView.findViewById(R.id.textViewP);
        textViewP.setText(context.getString(R.string.P) + " : " + Double.toString(recordInDiary.getProteins()));

        TextView textViewF = (TextView) convertView.findViewById(R.id.textViewF);
        textViewF.setText(context.getString(R.string.F) + " : " + Double.toString(recordInDiary.getFats()));

        TextView textViewC = (TextView) convertView.findViewById(R.id.textViewC);
        textViewC.setText(context.getString(R.string.C) + " : " + Double.toString(recordInDiary.getCarbohydrates()));

        LinearLayout layoutForDishes = (LinearLayout) convertView.findViewById(R.id.layoutForDishes);
        layoutForDishes.removeAllViews();

        for (String dish : recordInDiary.getDishes()) {

            TextView textView = new TextView(context);
            ViewGroup.LayoutParams layoutParams =
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(layoutParams);
            textView.setText(dish);
            textView.setTextAppearance(context, android.R.style.TextAppearance_DeviceDefault_Medium);
            layoutForDishes.addView(textView);

        }

        ((TextView) convertView.findViewById(R.id.textViewDate))
                .setText(simpleDateFormat.format(recordInDiary.getDate()));

        return convertView;
    }

    private int getMealName(float partOfDay) {

        if (partOfDay == 0) return R.string.breakfast;
        else if (partOfDay == .25f) return R.string.brunch;
        else if (partOfDay == .5f) return R.string.lunch;
        else if (partOfDay == .75f) return R.string.dinner;
        else if (partOfDay == 1) return R.string.supper;

        return R.string.breakfast;
    }
}
