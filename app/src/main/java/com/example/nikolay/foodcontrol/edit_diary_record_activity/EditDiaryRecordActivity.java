package com.example.nikolay.foodcontrol.edit_diary_record_activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.DietProgramMeal;
import com.example.nikolay.entities.DietType;
import com.example.nikolay.entities.MealRecordInDiary;
import com.example.nikolay.foodcontrol.App;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.usecases.UseCases;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EditDiaryRecordActivity extends Activity {

    public static final String EDITED_RECORD_KEY = "edited record";
    public static final String EDITED_RECORD_PROGRAM_KEY = "program";
    public static final String CURRENT_MEAL_NUMBER_KEY = "cureent meal number";

    private UseCases useCases;

    private Spinner spinner;
    private Button setDateBtn;
    private Button addDishBtn;
    private ImageButton saveBtn;
    private ProgressBar progressBar;
    private ScrollView scrollView;
    private EditText editP;
    private EditText editF;
    private EditText editC;
    private LinearLayout layoutForDishes;

    private ArrayList<String> mealNames = new ArrayList<>();
    private ArrayAdapter<String> spinnerAdapter;

    private Date recordDate;
    private ArrayList<DietProgramMeal> mealsOfActiveDietProgram;
    private int numberOfCurrentMeal;
    private MealRecordInDiary editedRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_diary_record);

        useCases = ((App) getApplication()).getUseCases();

        progressBar = (ProgressBar) findViewById(R.id.progressBar5);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  mealNames);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (editedRecord == null) {
                    numberOfCurrentMeal = position;
                    createContent(numberOfCurrentMeal);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setDateBtn = (Button) findViewById(R.id.setDateButton);
        setDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        editP = ((EditText) findViewById(R.id.editP));
        editF = ((EditText) findViewById(R.id.editF));
        editC = ((EditText) findViewById(R.id.editC));

        layoutForDishes = (LinearLayout) findViewById(R.id.layoutForDishes);

        addDishBtn = (Button) findViewById(R.id.addDishButton);
        addDishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEditDish();
            }
        });

        saveBtn = (ImageButton) findViewById(R.id.saveRecordButton);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDiaryRecord();
            }
        });

        editedRecord = getIntent().getParcelableExtra(EDITED_RECORD_KEY);

        if (editedRecord == null) { //add new record in diary
            numberOfCurrentMeal = getIntent().getIntExtra(CURRENT_MEAL_NUMBER_KEY, 0);
            ActiveDietProgramMealsGetter mealsGetter = new ActiveDietProgramMealsGetter();
            mealsGetter.execute();
        } else { //edit existing record
            createContent(editedRecord);
        }

    }

    private void createContent(MealRecordInDiary editedRecord) {
        recordDate = editedRecord.getDate();
        DietProgram dietProgram = getIntent().getParcelableExtra(EDITED_RECORD_PROGRAM_KEY);
        ArrayList<String> mealNames = new ArrayList<>();
        if (dietProgram.getType() == DietType.FiveTimes) {
            mealNames.add(getResources().getString(R.string.breakfast));
            mealNames.add(getResources().getString(R.string.brunch));
            mealNames.add(getResources().getString(R.string.lunch));
            mealNames.add(getResources().getString(R.string.dinner));
            mealNames.add(getResources().getString(R.string.supper));
        } else {
            mealNames.add(getResources().getString(R.string.breakfast));
            mealNames.add(getResources().getString(R.string.lunch));
            mealNames.add(getResources().getString(R.string.supper));
        }
        this.mealNames.clear();
        this.mealNames.addAll(mealNames);
        spinnerAdapter.notifyDataSetChanged();
        spinner.setSelection(editedRecord.getNumberOfMealInDay());

        editP.setText(Double.toString(editedRecord.getProteins()));
        editF.setText(Double.toString(editedRecord.getFats()));
        editC.setText(Double.toString(editedRecord.getCarbohydrates()));

        layoutForDishes.removeAllViewsInLayout();

        for (final String dish : editedRecord.getDishes()) {

            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            EditText editText = new EditText(this);
            editText.setLayoutParams(layoutParams);
            editText.setText(dish);
            editText.setId(dish.hashCode());
            layoutForDishes.addView(editText);
        }
    }

    private void createContent(int numberOfMealInDay) {
        DietProgramMeal dietProgramMeal = mealsOfActiveDietProgram.get(numberOfMealInDay);
        recordDate = new Date();
        ArrayList<String> mealNames = new ArrayList<>();
        if (useCases.getActiveDietProgram().getType() == DietType.FiveTimes) {
            mealNames.add(getResources().getString(R.string.breakfast));
            mealNames.add(getResources().getString(R.string.brunch));
            mealNames.add(getResources().getString(R.string.lunch));
            mealNames.add(getResources().getString(R.string.dinner));
            mealNames.add(getResources().getString(R.string.supper));
        } else {
            mealNames.add(getResources().getString(R.string.breakfast));
            mealNames.add(getResources().getString(R.string.lunch));
            mealNames.add(getResources().getString(R.string.supper));
        }
        this.mealNames.clear();
        this.mealNames.addAll(mealNames);
        spinnerAdapter.notifyDataSetChanged();
        spinner.setSelection(numberOfMealInDay);

        editP.setText(Double.toString(dietProgramMeal.getProteins()));
        editF.setText(Double.toString(dietProgramMeal.getFats()));
        editC.setText(Double.toString(dietProgramMeal.getCarbohydrates()));

        layoutForDishes.removeAllViewsInLayout();

        for (final String dish : dietProgramMeal.getDishes()) {

            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            EditText editText = new EditText(this);
            editText.setLayoutParams(layoutParams);
            editText.setText(dish);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equals(dish)) {
                        editP.setText("");
                        editF.setText("");
                        editC.setText("");
                    }
                }
            });
            editText.setId(dish.hashCode());
            layoutForDishes.addView(editText);
        }
    }

    private void showDateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                recordDate = calendar.getTime();
            }
        };

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(recordDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, listener, year, month, day);
        dialog.show();
    }

    private void showProgressBar(boolean show) {
        if (show) {
            scrollView.setVisibility(View.INVISIBLE);
            setDateBtn.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            setDateBtn.setVisibility(View.VISIBLE);
        }
    }

    private void addEditDish() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        EditText editText = new EditText(EditDiaryRecordActivity.this);
        editText.setLayoutParams(layoutParams);
        layoutForDishes.addView(editText);
        editText.requestFocus();
        if (editedRecord == null) {
            editP.setText("");
            editF.setText("");
            editC.setText("");
        }
    }

    private void saveDiaryRecord() {
        Double p;
        double f;
        double c;

        try {
            p = Double.parseDouble(editP.getText().toString());
        } catch (NumberFormatException nfe) {
            Toast.makeText(this, R.string.diary_notice_for_proteins, Toast.LENGTH_LONG).show();
            return;
        }

        try {
            f = Double.parseDouble(editF.getText().toString());
        } catch (NumberFormatException nfe) {
            Toast.makeText(this, R.string.diary_notice_for_fats, Toast.LENGTH_LONG).show();
            return;
        }

        try {
            c = Double.parseDouble(editC.getText().toString());
        } catch (NumberFormatException nfe) {
            Toast.makeText(this, R.string.diary_notice_for_carbohydrates, Toast.LENGTH_LONG).show();
            return;
        }

        ArrayList<String> dishes = new ArrayList<>();

        for (int i = 0; i < layoutForDishes.getChildCount(); i++) {
            EditText editDish = (EditText) layoutForDishes.getChildAt(i);
            dishes.add(editDish.getText().toString());
        }

        int numberOfMealInDay = spinner.getSelectedItemPosition();

        if (editedRecord == null) {
            MealRecordInDiary recordInDiary = new MealRecordInDiary(useCases.getActiveDietProgram().getUuid(),
                    numberOfMealInDay, recordDate, dishes, p, c, f);
            DiaryRecordSaver saver = new DiaryRecordSaver(DiaryRecordSaver.SAVE_TASK);
            saver.execute(recordInDiary);
        } else {
            MealRecordInDiary recordInDiary = new MealRecordInDiary(editedRecord.getRecordId(),
                    editedRecord.getDietProgramId(), numberOfMealInDay, recordDate, dishes, p, c, f);
            DiaryRecordSaver saver = new DiaryRecordSaver(DiaryRecordSaver.UPDATE_TASK);
            saver.execute(recordInDiary);
        }

    }

    class ActiveDietProgramMealsGetter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DietProgram dietProgram = useCases.getActiveDietProgram();
            mealsOfActiveDietProgram = useCases.getMealsForDietProgram(dietProgram.getUuid());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            createContent(numberOfCurrentMeal);
            showProgressBar(false);
        }
    }

    class DiaryRecordSaver extends AsyncTask<MealRecordInDiary, Void, Void> {

        static final int SAVE_TASK = 0;
        static final int UPDATE_TASK = 1;

        int task;

        public DiaryRecordSaver(int task) {
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(MealRecordInDiary... params) {

            if (task == SAVE_TASK) useCases.addMealRecordInDiary(params[0]);
            else if (task == UPDATE_TASK) useCases.updateMealRecordInDiary(params[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }
}
