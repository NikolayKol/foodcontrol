package com.example.nikolay.foodcontrol;

import android.app.Application;
import android.util.Log;
import com.example.nikolay.usecases.UseCases;

/**
 * Created by nikolay on 02.09.15.
 */
public class App extends Application {

    private UseCases useCases;

    @Override
    public void onCreate() {
        super.onCreate();
        this.useCases = new UseCases(this);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e("uncaught exception", ex.getMessage());
            }
        });
    }

    public UseCases getUseCases() {
        return useCases;
    }
}
