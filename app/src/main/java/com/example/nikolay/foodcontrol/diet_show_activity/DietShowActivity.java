package com.example.nikolay.foodcontrol.diet_show_activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.DietProgramMeal;
import com.example.nikolay.foodcontrol.App;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.foodcontrol.diet_program_create_activity.DietProgramCreateActivity;
import com.example.nikolay.usecases.UseCases;

import java.util.ArrayList;
import java.util.UUID;

public class DietShowActivity extends Activity {

    public static final String KEY_FOR_SHOWED_PROGRAM = "showed program";

    UseCases useCases;

    ProgressBar progressBar;
    ScrollView scrollView;
    LinearLayout layoutForMeals;
    TextView dietNameView;

    private ArrayList<View> mealViews;

    ImageButton setThisProgramActiveBtn;
    ImageButton createNewProgramFromThisBtn;
    ImageButton removeDietProgramBtn;

    private DietProgram showedDietProgram;
    private ArrayList<DietProgramMeal> showedMeals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_show);

        useCases = ((App) getApplication()).getUseCases();

        progressBar = (ProgressBar) findViewById(R.id.progressBar4);

        scrollView = (ScrollView) findViewById(R.id.scrollViewForMealsShow);

        layoutForMeals = (LinearLayout) findViewById(R.id.layoutForMealShowElements);

        dietNameView = (TextView) findViewById(R.id.textViewDietName);

        String showedProgramId = getIntent().getStringExtra(KEY_FOR_SHOWED_PROGRAM);

        if (showedProgramId == null) finish();

        ContentGetter contentGetter = new ContentGetter();
        contentGetter.execute(showedProgramId);

        setThisProgramActiveBtn = (ImageButton) findViewById(R.id.setDietActiveBtn);
        setThisProgramActiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                useCases.setDietProgramActive(getShowedDietProgram());
                Toast.makeText(DietShowActivity.this, R.string.thisProgramIsActive, Toast.LENGTH_LONG).show();
                finish();
            }
        });

        createNewProgramFromThisBtn = (ImageButton) findViewById(R.id.createProgramBasedThisBtn);
        createNewProgramFromThisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietShowActivity.this, DietProgramCreateActivity.class);
                intent.putExtra(DietProgramCreateActivity.SAMPLE_DIET_PROGRAM_ID_KEY, getShowedDietProgram().getUuid().toString());
                startActivity(intent);
            }
        });

        removeDietProgramBtn = (ImageButton) findViewById(R.id.removeDietProgramBtn);
        removeDietProgramBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDietProgram();
            }
        });

    }

    private void removeDietProgram() {
        Remover remover = new Remover();
        remover.execute();
    }

    private void showProgressBar(Boolean show) {
        if (show) {
            scrollView.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    private void createContent() {
        dietNameView.setText(showedDietProgram.getName());

        mealViews = new ArrayList<View>();

        for (int i = 0; i < showedDietProgram.getType().getTimes(); i++) {

            DietProgramMeal meal = showedMeals.get(i);
            View view = getLayoutInflater().inflate(R.layout.show_meal_element, null);

            TextView p = (TextView) view.findViewById(R.id.textViewP);
            p.setText(getResources().getString(R.string.P) + ": " + Double.toString(meal.getProteins()));

            TextView f = (TextView) view.findViewById(R.id.textViewF);
            f.setText(getResources().getString(R.string.F) + ": " + Double.toString(meal.getFats()));

            TextView c = (TextView) view.findViewById(R.id.textViewC);
            c.setText(getResources().getString(R.string.C) + ": " + Double.toString(meal.getCarbohydrates()));

            if (i != 0) {
                TextView interval = (TextView) view.findViewById(R.id.textViewI);
                interval.setVisibility(View.VISIBLE);
                interval.setText(showedDietProgram.getIntervals().get(i - 1));
                view.findViewById(R.id.textViewIntervalName).setVisibility(View.VISIBLE);
            }

            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layoutForDishesInShowActivity);

            for (String dish : meal.getDishes()) {

                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                TextView textView = new TextView(this);
                textView.setLayoutParams(params);
                textView.setTextAppearance(DietShowActivity.this, android.R.style.TextAppearance_DeviceDefault_Medium);
                textView.setText(dish);
                linearLayout.addView(textView);
            }

            layoutForMeals.addView(view);
            mealViews.add(view);
        }

        setMealNames();
    }

    private void setMealNames() {
        if (showedDietProgram.getType().getTimes() == 5) {

            for (int i = 0; i < 5; i++) {
                TextView nameView = (TextView) mealViews.get(i).findViewById(R.id.textViewMealName);
                switch (i) {
                    case 0:
                        nameView.setText(R.string.breakfast);
                        break;
                    case 1:
                        nameView.setText(R.string.brunch);
                        break;
                    case 2:
                        nameView.setText(R.string.lunch);
                        break;
                    case 3:
                        nameView.setText(R.string.dinner);
                        break;
                    case 4:
                        nameView.setText(R.string.supper);
                        break;
                }

            }
        } else {
            for (int i = 0; i < 3; i++) {
                TextView nameView = (TextView) mealViews.get(i).findViewById(R.id.textViewMealName);
                switch (i) {
                    case 0:
                        nameView.setText(R.string.breakfast);
                        break;
                    case 1:
                        nameView.setText(R.string.lunch);
                        break;
                    case 2:
                        nameView.setText(R.string.supper);
                        break;
                }
            }
        }
    }

    public DietProgram getShowedDietProgram() {
        return showedDietProgram;
    }

    class ContentGetter extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(String... params) {
            showedDietProgram = useCases.getDietProgram(UUID.fromString(params[0]));
            showedMeals = useCases.getMealsForDietProgram(UUID.fromString(params[0]));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            createContent();
            showProgressBar(false);
        }
    }

    class Remover extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            useCases.hideDietProgram(showedDietProgram.getUuid());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }
}
