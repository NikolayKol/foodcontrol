package com.example.nikolay.foodcontrol.archive_activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.foodcontrol.App;
import com.example.nikolay.foodcontrol.R;
import com.example.nikolay.usecases.UseCases;

import java.util.ArrayList;

public class ArchiveActivity extends Activity {

    ListView dietList;
    ProgressBar progressBar;
    ArrayList<DietProgram> dietPrograms;
    ArchiveListAdapter archiveListAdapter;
    UseCases useCases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_archive);

        useCases = ((App) getApplication()).getUseCases();

        dietList = (ListView) findViewById(R.id.archiveListView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar3);

        archiveListAdapter = new ArchiveListAdapter(this);

        dietList.setAdapter(archiveListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DietProgramsGetter dietProgramsGetter = new DietProgramsGetter();
        dietProgramsGetter.execute();
    }

    private void showProgressBar(boolean show) {
        if (show) {
            dietList.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            dietList.setVisibility(View.VISIBLE);
        }
    }

    class DietProgramsGetter extends AsyncTask <Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            dietPrograms = useCases.getVisibleDietPrograms();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            archiveListAdapter.setDietPrograms(dietPrograms);
            archiveListAdapter.notifyDataSetChanged();
            showProgressBar(false);
        }
    }
}
