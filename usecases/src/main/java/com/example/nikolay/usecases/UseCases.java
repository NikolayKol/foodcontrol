package com.example.nikolay.usecases;

import android.content.Context;

import com.example.nikolay.entities.DietProgram;
import com.example.nikolay.entities.DietProgramMeal;
import com.example.nikolay.entities.MealRecordInDiary;
import com.example.nikolay.storage.Storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by nikolay on 06.09.15.
 */
public class UseCases {

    private Storage storage;

    public UseCases(Context context) {
        storage = new Storage(context);
    }

    public boolean isFirstAppUsing() {

        return storage.getActiveDietProgram() == null;
    }

    public void setDietProgramActive(DietProgram currentDietProgram) {
        storage.setDietProgramActive(currentDietProgram);
    }

    public DietProgram getDietProgram(UUID uuid) {
        return storage.getDietProgram(uuid);
    }

    public DietProgram getActiveDietProgram() {
        return storage.getActiveDietProgram();
    }

    public ArrayList<DietProgramMeal> getMealsForDietProgram(UUID uuid) {
        return storage.getMealsForDietProgram(uuid);
    }

    public ArrayList<DietProgram> getVisibleDietPrograms() {
        return storage.getVisibleDietPrograms();
    }

    public ArrayList<MealRecordInDiary> getDiaryRecordsForDietProgram(UUID programId) {
        return storage.getDiaryRecordsForDietProgram(programId);
    }

    public Map<MealRecordInDiary, DietProgram> getDiaryData() {

        Map<MealRecordInDiary, DietProgram> data = new HashMap<>();

        ArrayList<MealRecordInDiary> records = storage.getAllDiaryRecords();

        DietProgram programOfRecord = null;

        for (MealRecordInDiary recordInDiary : records) {

            if (programOfRecord == null || !programOfRecord.getUuid().equals(recordInDiary.getDietProgramId())) {
                programOfRecord = storage.getDietProgram(recordInDiary.getDietProgramId());
            }

            data.put(recordInDiary, programOfRecord);

        }

        return data;
    }

    public void addDietProgramToStorage(DietProgram dietProgram, ArrayList<DietProgramMeal> meals, boolean active) {

        storage.addDietProgram(dietProgram, meals, active);
    }

    public void addMealRecordInDiary(MealRecordInDiary recordInDiary) {
        storage.addMealRecordInDiary(recordInDiary);
    }

    public void updateMealRecordInDiary(MealRecordInDiary recordInDiary) {
        storage.updateMealRecordInDiary(recordInDiary);
    }

    public void hideDietProgram(UUID uuid) {

        if (getActiveDietProgram().getUuid().equals(uuid)) {

            ArrayList<DietProgram> programs = getVisibleDietPrograms();
            if (programs.size() == 1) setDietProgramActive(null);
            else {
                int i = 0;

                while (programs.get(i).getUuid().equals(uuid)) i++;

                setDietProgramActive(programs.get(i));
            }
        }

        if (getDiaryRecordsForDietProgram(uuid).isEmpty())
            storage.removeDietProgram(uuid);
        else storage.hideDietProgram(uuid);
    }
}
